package com.fix305.example.jib

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController
import kotlin.random.Random

@SpringBootApplication
class JibExampleApplication

fun main(args: Array<String>) {
    runApplication<JibExampleApplication>(*args)
}

@RestController
class ExampleController {
    @GetMapping
    fun hello(): ResponseEntity<String> {
        return ResponseEntity.ok("Hello, it's jib example! Here some random long:" + Random.nextLong())
    }
}