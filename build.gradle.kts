plugins {
    id("org.springframework.boot") version "3.3.1"
    id("io.spring.dependency-management") version "1.1.5"
    kotlin("jvm") version "1.9.24"
    kotlin("plugin.spring") version "1.9.24"

    id("com.google.cloud.tools.jib") version "3.4.3"
}

group = "com.fix305"
version = "0.0.6"

jib {
    val registry = System.getenv("CI_REGISTRY_IMAGE")

    from {
        image = "openjdk:17-slim-buster"
        /*platforms { // macos m1 compatibility
            platform {
                architecture = "arm64"
                os = "linux"
            }
        }*/
    }

    to {
        if (registry != null) {
            val branch = System.getenv("CI_COMMIT_BRANCH") ?: System.getenv("CI_MERGE_REQUEST_SOURCE_BRANCH_NAME")
            val defaultBranch = System.getenv("CI_DEFAULT_BRANCH")

            val suffix = if (branch == defaultBranch) "" else "-$branch"

            image = registry + ":" + project.version + suffix

            auth {
                username = System.getenv("CI_REGISTRY_USER") ?: ""
                password = System.getenv("CI_REGISTRY_PASSWORD") ?: ""
            }
        } else {
            image = project.name + ":" + project.version + "-local"
        }
    }
}

java {
    toolchain {
        languageVersion = JavaLanguageVersion.of(17)
    }
}

repositories {
    mavenCentral()
}

dependencies {
    implementation("org.springframework.boot:spring-boot-starter-web")
    implementation("com.fasterxml.jackson.module:jackson-module-kotlin")
    implementation("org.jetbrains.kotlin:kotlin-reflect")
    testImplementation("org.springframework.boot:spring-boot-starter-test")
    testImplementation("org.jetbrains.kotlin:kotlin-test-junit5")
    testRuntimeOnly("org.junit.platform:junit-platform-launcher")
}

kotlin {
    compilerOptions {
        freeCompilerArgs.addAll("-Xjsr305=strict")
    }
}

tasks.withType<Test> {
    useJUnitPlatform()
}
